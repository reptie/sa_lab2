import numpy as np


class Function(object):
    """Class with implemented grads and hessians of the given function"""
    def __init__(self, function, xs_amount=3):
        print('Function constructor start')
        self.function = function
        self.xs_amount = xs_amount
        print('Function constructor end')

    def __call__(self, vector):
        return self.function(vector)

    def grad_f(self, custom_function=None):
        func = self.function if custom_function is None else custom_function

        def df_dxall(mask, h):
            def df_dxcurrent(point):
                return (func(np.where(mask, point + h, point)) - func(np.where(mask, point - h, point))) / 2 / h

            return df_dxcurrent

        return df_dxall

    def grad(self, point, h=0.00001):
        grad_function = self.grad_f()
        mask = np.array([False] * self.xs_amount)

        grad_arr = []
        for index in range(self.xs_amount):
            mask[index] = True
            grad_arr.append(grad_function(mask, h)(point))
            mask[index] = False
        return np.array(grad_arr, dtype=np.float64)

    def hessian(self, point, h=0.00001):
        grad_function = self.grad_f()

        x_i = np.array([False] * self.xs_amount)
        x_j = x_i.copy()

        hessian = []
        for i in range(self.xs_amount):
            x_i[i] = True
            d2f_dxid = self.grad_f(grad_function(x_i, h))

            hessian_row = []
            for j in range(self.xs_amount):
                x_j[j] = True
                hessian_row.append(self.grad_f(grad_function(x_i, h))(x_j, h)(point))
                x_j[j] = False

            x_i[i] = False
            hessian.append(hessian_row)
        return np.array(hessian)


class ConjugateGradient:
    """Class that calculates local minimum a given function"""
    def __init__(self, func):
        """Constructor

        :param func: Function
        """
        print('Non lin constructor start')
        self.f = func
        print('None lin constructor end')

    def alpha_with_md(self, point, h, step=0.15, eps=0.1e-8, eps_=1e-5):
        alpha_k = 1

        grad = self.f.grad(point)
        while self.f(point - alpha_k * h) - self.f(point) > eps * alpha_k * np.dot(grad, h):
            alpha_k *= step
            if alpha_k < eps_: break

        return alpha_k

    def alpha_with_mns(self, point, h, eps=1e-3):
        left, right, phi = 0, 1000, (np.sqrt(5) + 1) / 2

        while abs(right - left) >= eps:
            mid = (right - left) / phi
            if self.f(point - (right - mid) * h) >= self.f(point - (left + mid) * h):
                left = right - mid
            else:
                right = left + mid
        return (right + left) / 2

    def get_alpha(self, method_to_use):
        if method_to_use == 'md':
            return self.alpha_with_md
        else:
            return self.alpha_with_mns

    def non_lin_conj_grad(self, start_point, eps=1e-1, alpha_custom=0, method_to_use='d', epochs=1000000, quad=False):
        if not alpha_custom:
            alpha_method = self.get_alpha(method_to_use)

        argmin = start_point.copy()
        print('argmin:', argmin)
        h = self.f.grad(argmin)
        print('grad:', h)

        for epoch in range(epochs):
            hessian = self.f.hessian(argmin)
            beta = np.dot(self.f.grad(argmin), hessian @ h) / np.dot(h, hessian @ h) if epoch % self.f.xs_amount else 0

            x_prev = argmin.copy()

            h = self.f.grad(x_prev) - beta * h

            alpha = alpha_method(x_prev, h) if not alpha_custom else alpha_custom
            argmin -= alpha * h

            print('current argmin:', argmin.shape, '\n', argmin, '\n\n')
            if (np.linalg.norm(argmin - x_prev) < eps) or (np.linalg.norm(self.f.grad(argmin)) < eps): return argmin
            if quad and epoch == self.f.xs_amount: return argmin
        return argmin
