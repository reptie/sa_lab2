from os import path

DIR_PATH = path.dirname(path.realpath(__file__))
DIR_PATH = DIR_PATH[:DIR_PATH.rfind('\\') + 1]

UIS_PATH = DIR_PATH + 'template\\'

MAIN_BLOCK_UI_FILE_NAME = 'main_block.ui'
FILES_MANAGE_UI_FILE_NAME = 'files_manage.ui'
PLOTS_UI_FILE_NAME = 'plots.ui'
