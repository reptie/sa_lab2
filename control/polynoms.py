import numpy as np


class Polynomial(object):
    """Parent class for all polynomial types"""
    def __init__(self, degree, point=np.array([0.0]), pol_letter=''):
        """Constructor

        :param degree: int, max polynomial degree
        :param point: np.array[float]
        :param pol_letter: str
        """
        self.degrees = np.arange(degree + 1)
        self.x = point
        self.point = point.astype(str)
        self.pol_letter = pol_letter

    def __repr__(self):
        """Implementation of str representation of the class"""
        # TODO: reimplement
        pass
        letters_points_degrees = list(map(lambda x: f'{self.pol_letter}{self.point}{x}', self.degrees))
        str_repr = ''.join(letters_points_degrees)
        if len(self.point) > 1:
            # x1^2 -> (x1)^2
            str_repr = str_repr.replace(self.point, '(' + self.point + ')')
        if self.pol_letter:
            # T(x1)^2 -> [T(x1)]^2
            str_repr = str_repr.replace(self.pol_letter, '[' + self.pol_letter)
            str_repr = str_repr.replace('^', ']^')
        return str_repr + ' '

    def _pow(self, x):
        """Makes a wonderful power up

        e.g. [1, 2] ^ [1, 2, 3] -> [[1, 1, 1], [2, 4, 8]] \\ [[1^1, 1^2, 1^3], [2^1, 2^2, 2^3]]
        """
        return np.hstack([x.reshape(-1, 1)] * self.degrees.size) ** self.degrees.reshape((1, -1))


class ChebyshevPolynomial(Polynomial):
    def __init__(self, degree, point, pol_degree, pol_letter='T'):
        """Implementation of Chebyshev Polynomial class

        :param degree: int, degree of x
        :param point: np.array[float], given x
        :param pol_degree: int, polynomial degree (e.g. T_1, T_2, etc)
        :param pol_letter: str
        """
        super().__init__(degree, point, pol_letter + f'_{pol_degree}')
        self.pol_degree = pol_degree

    def get_result(self) -> np.array:
        """Calculates [T_{pol_degree}(x)]^degree"""
        if self.pol_degree == 0: return self._pow(np.ones_like(self.x))
        if self.pol_degree == 1: return self._pow(self.x)

        ts = [lambda x: np.ones_like(x), lambda x: x]
        for degree in range(2, self.pol_degree):
            ts.append(lambda x: 2 * x * ts[-1](x) - ts[-2](x))
        return self._pow(ts[-1](self.x))


class LagguerePolynomial(Polynomial):
    def __init__(self, degree, point, pol_degree, pol_letter='L'):
        """Implementation of Lagguere Polynomial class

        :param degree: int, degree of x
        :param point: float, given x
        :param pol_degree: int, polynomial degree (e.g. L_1, L_2, etc)
        :param pol_letter: str
        """
        super().__init__(degree, point, pol_letter + f'_{pol_degree}')
        self.pol_degree = pol_degree

    def get_result(self) -> np.array:
        """Calculates [L_{pol_degree}(x)]^degree"""
        if self.pol_degree == 0: return self._pow(np.ones_like(self.x))
        if self.pol_degree == 1: return self._pow(1 - self.x)

        ls = [lambda x: np.ones_like(x), lambda x: 1 - x]
        for degree in range(2, self.pol_degree):
            ls.append(lambda x: ((2 * degree - 1 - x) * ls[-1](x) - (degree - 1) * ls[-2](x)) / degree)
        return self._pow(ls[-1](self.x))


class LegendrePolynomial(Polynomial):
    def __init__(self, degree, point, pol_degree, pol_letter='P'):
        """Implementation of Legendre Polynomial class

        :param degree: int, degree of x
        :param point: float, given x
        :param pol_degree: int, polynomial degree (e.g. P_1, P_2, etc)
        :param pol_letter: str
        """
        super().__init__(degree, point, pol_letter + f'_{pol_degree}')
        self.pol_degree = pol_degree

    def get_result(self) -> np.array:
        """Calculates [P_{pol_degree}(x)]^degree"""
        if self.pol_degree == 0: return self._pow(np.ones_like(self.x))
        if self.pol_degree == 1: return self._pow(self.x)

        ps = [lambda x: np.ones_like(x), lambda x: x]
        for degree in range(2, self.pol_degree):
            ps.append(lambda x: ((2 * degree - 1) * x * ps[-1](x) - (degree - 1) * ps[-2](x)) / degree)
        return self._pow(ps[-1](self.x))


class HermitePolynomial(Polynomial):
    def __init__(self, degree, point, pol_degree, pol_letter='H'):
        """Implementation of Hermite Polynomial class

        :param degree: int, degree of x
        :param point: float, given x
        :param pol_degree: int, polynomial degree (e.g. H_1, H_2, etc)
        :param pol_letter: str
        """
        super().__init__(degree, point, pol_letter + f'_{pol_degree}')
        self.pol_degree = pol_degree

    def get_result(self) -> np.array:
        """Calculates [H_{pol_degree}(x)]^degree"""
        if self.pol_degree == 0: return self._pow(np.ones_like(self.x))
        if self.pol_degree == 1: return self._pow(self.x)

        hs = [lambda x: np.ones_like(x), lambda x: x]
        for degree in range(2, self.pol_degree):
            hs.append(lambda x: x * hs[-1](x) - (degree - 1) * hs[-2](x))
        return self._pow(hs[-1](self.x))
