import numpy as np

from control.non_linear_method_solver import Function, ConjugateGradient
from control.polynoms import ChebyshevPolynomial, LegendrePolynomial, LagguerePolynomial, HermitePolynomial


class ProblemSolver:
    """Ultimate pizdec protiv superboss le-Pankratove (backend). S pivkom pokatit"""
    def __init__(self, sizes, powers, poly_type, input_filepath, output_filepath):
        """Constructor
        
        :param sizes: dict, {'x1': int, 'x2': int, 'x3': int, 'y': int}
        :param powers: dict, {'x1': int, 'x2': int, 'x3': int}
        :param poly_type: str, either 'Chebyshev', 'Legendre', 'Lagguere', or 'Hermite'
        :param input_filepath: str
        :param output_filepath: str
        """
        self._size_x1, self._size_x2, self._size_x3, self._size_y = sizes['x1'], sizes['x2'], sizes['x3'], sizes['y']
        self._power_x1, self._power_x2, self._power_x3 = powers['x1'], powers['x2'], powers['x3']

        self._polynomial = None
        if poly_type == 'Chebyshev': self._polynomial = ChebyshevPolynomial
        elif poly_type == 'Legendre': self._polynomial = LegendrePolynomial
        elif poly_type == 'Lagguere': self._polynomial = LagguerePolynomial
        elif poly_type == 'Hermite': self._polynomial = HermitePolynomial

        data = np.loadtxt(input_filepath)
        assert data.shape[-1] == sum([self._size_x1, self._size_x2, self._size_x3, self._size_y])

        self._xs, self.y = data[:, :-self._size_y], data[:, -self._size_y:]
        if self._size_y == 1: self.y = self.y.reshape((1, -1))

        self._output_filepath = output_filepath

    def _system_maker(self, table, power):
        """Makes system based on given values"""
        system = np.zeros(len(table)).reshape((-1, 1))
        for degree, x_ij in enumerate(table.T):
            polynomial = self._polynomial(power, x_ij, degree + 1)
            system = np.hstack([system, polynomial.get_result()])
        return system[:, 1:]

    @staticmethod
    def _system_solver(system, y):
        """Solves an equation system with given non-linear method (conjugate_gradient for variant 1)"""
        def function(system_, y_):
            return lambda x: ((system_ * x.reshape(system_.shape) - y_.reshape((-1, 1))) ** 2).sum()

        print('System solver:')
        print(system, '\n', y)
        func = Function(function(system, y), xs_amount=system.size)
        non_linear_solver = ConjugateGradient(func)

        result = non_linear_solver.non_lin_conj_grad(start_point=np.zeros(system.size), method_to_use='mns', quad=True)
        return result.reshape(system.shape)

    def main_solver(self):
        """Ultimate solver for pizdec"""
        y_calculated = self.y.copy().T
        print('started')
        for i, y in enumerate(self.y.T):
            # second table maker
            second_table = np.zeros((3, len(self._xs)))
            start_column, end_column = 0, self._size_x1
            for next_border, power, i_ in zip([self._size_x2, self._size_x3],
                                              [self._power_x1, self._power_x2, self._power_x3],
                                              list(range(3))):

                table = self._xs[:, start_column:end_column]
                print(table)
                system = self._system_maker(table, power)
                print(system)
                print(self._xs.shape, y.shape, system.shape)
                found_coeffs = self._system_solver(system, y)
                print('here5')
                second_table[i_] = (system * found_coeffs).sum(axis=1)
                print('here6')

                start_column = end_column
                end_column += next_border

            # y^ =
            second_table = second_table.T
            second_system = self._system_maker(second_table, power=1)
            second_coeffs = self._system_solver(second_system, y)
            y_calculated[i] = (second_system * second_coeffs).sum(axis=1)
        return y_calculated.T, self.y
