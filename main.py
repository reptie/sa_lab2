from PyQt5 import QtWidgets
import sys

from .control.ui_manager import CalculatorUI


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = CalculatorUI(MainWindow)
    ui.setup_ui(MainWindow)
    MainWindow.show()
