from PyQt5 import QtCore, QtWidgets
from os import environ
import sys

from control.problem_solver import ProblemSolver

environ['QT_DEBUG_PLUGINS'] = '1'


class CalculatorUI(object):
    """Ultimate pizdec protiv superboss le-Pankratove (frontend). S pivkom pokatit"""
    def __init__(self, main_window):

        self.DEFAULT_WINDOW_NAME = 'Laba2-Kapinus/Ngo/Chemanova'

        self.central_widget = QtWidgets.QWidget(main_window)

        self.results = QtWidgets.QTextBrowser(self.central_widget)

        self.input_data_path_text = QtWidgets.QTextBrowser(self.central_widget)
        self.output_data_path_text = QtWidgets.QTextBrowser(self.central_widget)

        self.power_x1 = QtWidgets.QSpinBox(self.central_widget)
        self.power_x2 = QtWidgets.QSpinBox(self.central_widget)
        self.power_x3 = QtWidgets.QSpinBox(self.central_widget)

        self.size_x1 = QtWidgets.QSpinBox(self.central_widget)
        self.size_x2 = QtWidgets.QSpinBox(self.central_widget)
        self.size_x3 = QtWidgets.QSpinBox(self.central_widget)
        self.size_y = QtWidgets.QSpinBox(self.central_widget)

        self.input_data_button = QtWidgets.QPushButton(self.central_widget)
        self.output_data_button = QtWidgets.QPushButton(self.central_widget)

        self.execute_button = QtWidgets.QPushButton(self.central_widget)
        self.plot_button = QtWidgets.QPushButton(self.central_widget)

        self.chebyshev_button = QtWidgets.QRadioButton(self.central_widget)
        self.legendre_button = QtWidgets.QRadioButton(self.central_widget)
        self.lagguere_button = QtWidgets.QRadioButton(self.central_widget)
        self.hermite_button = QtWidgets.QRadioButton(self.central_widget)

        self.result_label = QtWidgets.QLabel(self.central_widget)
        self.input_data_label = QtWidgets.QLabel(self.central_widget)
        self.output_data_label = QtWidgets.QLabel(self.central_widget)
        self.poly_looks_label = QtWidgets.QLabel(self.central_widget)
        self.x1_label = QtWidgets.QLabel(self.central_widget)
        self.x2_label = QtWidgets.QLabel(self.central_widget)
        self.x3_label = QtWidgets.QLabel(self.central_widget)
        self.poly_powers_label = QtWidgets.QLabel(self.central_widget)
        self.vectors_label = QtWidgets.QLabel(self.central_widget)
        self.size_x1_label = QtWidgets.QLabel(self.central_widget)
        self.size_x2_label = QtWidgets.QLabel(self.central_widget)
        self.size_x3_label = QtWidgets.QLabel(self.central_widget)
        self.size_y_label = QtWidgets.QLabel(self.central_widget)

        self.first_long_line = QtWidgets.QFrame(self.central_widget)
        self.second_long_line = QtWidgets.QFrame(self.central_widget)
        self.third_long_line = QtWidgets.QFrame(self.central_widget)
        self.fourth_long_line = QtWidgets.QFrame(self.central_widget)
        self.first_short_line = QtWidgets.QFrame(self.central_widget)
        self.second_short_line = QtWidgets.QFrame(self.central_widget)
        self.third_short_line = QtWidgets.QFrame(self.central_widget)

        self.input_data_path, self.output_data_path = '', ''

    def setup_main_window(self, main_window):
        main_window.setObjectName(self.DEFAULT_WINDOW_NAME)
        main_window.resize(450, 510)
        main_window.setMinimumSize(QtCore.QSize(450, 510))
        main_window.setMaximumSize(QtCore.QSize(450, 510))
        main_window.setWindowTitle(QtCore.QCoreApplication.translate(self.DEFAULT_WINDOW_NAME,
                                                                     self.DEFAULT_WINDOW_NAME))

        self.central_widget.setObjectName('central_widget')

    def setup_property(self, self_property, property_name, property_sizes_tuple, property_label=''):
        self_property.setGeometry(QtCore.QRect(*property_sizes_tuple))
        self_property.setObjectName(property_name)
        if property_label:
            self_property.setText(QtCore.QCoreApplication.translate(self.DEFAULT_WINDOW_NAME, property_label))

    @staticmethod
    def setup_line(line):
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)

    @staticmethod
    def setup_button(button, method_on_clicked):
        button.clicked.connect(method_on_clicked)

    def choose_input_file(self):
        chosen_file, _ = QtWidgets.QFileDialog.getOpenFileName(filter='Text files (*.txt)')
        self.input_data_path_text.setText(chosen_file[chosen_file.rfind('/')+1:])
        self.input_data_path = chosen_file

    def choose_output_file(self):
        chosen_file, _ = QtWidgets.QFileDialog.getSaveFileName(filter='Text files (*.txt)')
        self.output_data_path_text.setText(chosen_file[chosen_file.rfind('/')+1:])
        self.output_data_path = chosen_file

    def all_variables_set(self):
        msg = None
        #if not self.input_data_path:
        #    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
        #                                'Parameter is not set error',
        #                                f'Parameter {"input data path"} is not set. '
        #                                f'Please enter it and try again!')
        #elif not self.output_data_path:
        #    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
        #                                'Parameter is not set error',
        #                                f'Parameter {"output data path"} is not set. '
        #                                f'Please enter it and try again!')
        #
        #elif not self.size_x1.value():
        #    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
        #                                'Parameter is not set error',
        #                                f'Parameter {"size x1"} is not set. '
        #                                f'Please enter it and try again!')
        #
        #elif not self.size_x2.value():
        #    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
        #                                'Parameter is not set error',
        #                                f'Parameter {"size x2"} is not set. '
        #                                f'Please enter it and try again!')
        #
        #elif not self.size_x3.value():
        #    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
        #                                'Parameter is not set error',
        #                                f'Parameter {"size x3"} is not set. '
        #                                f'Please enter it and try again!')
        #
        #elif not self.size_y.value():
        #    msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
        #                                'Parameter is not set error',
        #                                f'Parameter {"size y"} is not set. '
        #                                f'Please enter it and try again!')
        if msg:
            msg.show()
            msg.raise_()
            msg.exec_()
        else: self.execution_result()

    def execution_result(self):
        poly_type = 'Chebyshev'
        if self.legendre_button.isChecked():
            poly_type = 'Legendre'
        elif self.lagguere_button.isChecked():
            poly_type = 'Lagguere'
        elif self.hermite_button.isChecked():
            poly_type = 'Hermite'

       #parameters = ({'x1': self.size_x1.value(),
        #               'x2': self.size_x2.value(),
        #               'x3': self.size_x3.value(),
         #              'y': self.size_y.value()},
          #            {'x1': self.power_x1.value(),
           #            'x2': self.power_x2.value(),
            #           'x3': self.power_x3.value()},
             #         poly_type,
              #        self.input_data_path,
               #       self.output_data_path)

        parameters = ({'x1': 2,
                      'x2': 2,
                      'x3': 2,
                     'y': 5},
                     {'x1': 2,
                      'x2': 3,
                      'x3': 2},
                    poly_type,
                    '/home/maria/Documents/sa_lab2/data_exmp.txt',
                    '')

        try:
            solver = ProblemSolver(*parameters)
            y_calculated, y_true = solver.main_solver()
            print(y_calculated, y_true)
        except AssertionError:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
                                        'Wrong parameter set error',
                                        'Set parameters (size x1, size x2, size x3, size y) do not match'
                                        f'amount of columns in the file "{self.input_data_path}".'
                                        'Please enter it correctly and try again later!')
            msg.show()
            msg.raise_()
            msg.exec_()

    def setup_ui(self, main_window):
        self.setup_main_window(main_window)

        self.setup_property(self.results, 'results', (160, 30, 271, 461))

        self.setup_property(self.input_data_path_text, 'input_data_path', (10, 40, 111, 23))
        self.setup_property(self.output_data_path_text, 'output_data_path', (10, 90, 111, 23))

        self.setup_property(self.power_x1, 'power_x1', (10, 240, 42, 22))
        self.setup_property(self.power_x2, 'power_x2', (60, 240, 42, 22))
        self.setup_property(self.power_x3, 'power_x3', (110, 240, 42, 22))

        self.setup_property(self.size_x1, 'size_x1', (10, 320, 42, 22))
        self.setup_property(self.size_x2, 'size_x2', (10, 370, 42, 22))
        self.setup_property(self.size_x3, 'size_x3', (10, 420, 42, 22))
        self.setup_property(self.size_y, 'size_y', (90, 320, 42, 22))

        self.setup_property(self.input_data_button, 'input_data_button', (130, 40, 21, 23), '...')
        self.setup_property(self.output_data_button, 'output_data_button', (130, 90, 21, 23), '...')
        self.setup_button(self.input_data_button, self.choose_input_file)
        self.setup_button(self.output_data_button, self.choose_output_file)

        self.setup_property(self.execute_button, 'execute_button', (10, 470, 71, 23), 'Виконати')
        self.setup_property(self.plot_button, 'plot_button', (90, 470, 61, 23), 'Графік')
        self.setup_button(self.execute_button, self.all_variables_set)
        self.setup_button(self.plot_button, lambda: print('plot button was clicked'))

        self.setup_property(self.chebyshev_button, 'chebyshev_button', (10, 150, 82, 17), 'Чебишева')
        self.setup_property(self.legendre_button, 'legendre_button', (10, 170, 82, 17), 'Лежандра')
        self.setup_property(self.lagguere_button, 'lagguere_button', (90, 150, 82, 17), 'Лаггера')
        self.setup_property(self.hermite_button, 'hermite_button', (90, 170, 82, 17), 'Ерміта')
        self.chebyshev_button.setChecked(True)

        self.setup_property(self.result_label, 'result_label', (160, 10, 71, 16), 'Результати')
        self.setup_property(self.input_data_label, 'input_data_label', (10, 20, 111, 16), 'Файл вихідних даних')
        self.setup_property(self.output_data_label, 'output_data_label', (10, 70, 91, 16), 'Файл результатів')
        self.setup_property(self.poly_looks_label, 'poly_looks_label', (10, 120, 101, 16), 'Вигляд поліномів')
        self.setup_property(self.x1_label, 'x1_label', (10, 220, 41, 16), 'Для х1')
        self.setup_property(self.x2_label, 'x2_label', (60, 220, 41, 16), 'Для х2')
        self.setup_property(self.x3_label, 'x3_label', (110, 220, 41, 16), 'Для х3')
        self.setup_property(self.poly_powers_label, 'poly_powers_label', (10, 200, 91, 16), 'Степені поліномів')
        self.setup_property(self.vectors_label, 'vectors_label', (10, 270, 47, 13), 'Вектори')
        self.setup_property(self.size_x1_label, 'size_x1_label', (10, 300, 81, 16), 'Розмірність х1')
        self.setup_property(self.size_x2_label, 'size_x2_label', (10, 350, 81, 16), 'Розмірність х2')
        self.setup_property(self.size_x3_label, 'size_x3_label', (10, 400, 81, 16), 'Розмірність х3')
        self.setup_property(self.size_y_label, 'size_y_label', (90, 300, 81, 16), 'Розмірність у')

        self.setup_property(self.first_long_line, 'first_long_line', (0, 110, 161, 16))
        self.setup_property(self.second_long_line, 'second_long_line', (0, 190, 161, 16))
        self.setup_property(self.third_long_line, 'third_long_line', (0, 260, 161, 16))
        self.setup_property(self.fourth_long_line, 'fourth_long_line', (0, 450, 161, 16))
        self.setup_property(self.first_short_line, 'first_short_line', (0, 130, 101, 16))
        self.setup_property(self.second_short_line, 'second_short_line', (0, 210, 101, 16))
        self.setup_property(self.third_short_line, 'third_short_line', (0, 280, 101, 16))
        for line in [self.first_long_line, self.second_long_line, self.third_long_line, self.fourth_long_line,
                     self.first_short_line, self.second_short_line, self.third_short_line]:
            self.setup_line(line)

        main_window.setCentralWidget(self.central_widget)
        QtCore.QMetaObject.connectSlotsByName(main_window)
